//
//  bookModel.m
//  booksModelViewcontroller
//
//  Created by hcc on 2014/2/26.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "bookModel.h"
#import <AFNetworking.h>

@implementation bookModel

-(void)fetchBookList
{
    // 自己要會查 AFNetworking 網站
    AFHTTPRequestOperationManager* manager  = [AFHTTPRequestOperationManager manager];
    NSString* api = @"http://alphacamp.digik.tw/books.json";
    
    [manager GET:api parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"OK!, %@", responseObject);
        // 新寫法
        NSMutableArray* bookList = @[].mutableCopy;
        
        [responseObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {

            bookModel* book = [bookModel new];
            book.bookID = [obj[@"id"] integerValue];
            book.title = obj[@"title"];
            book.author = obj[@"name"];
            book.price = [obj[@"price"] integerValue];
            book.content = obj[@"description"];
            book.coverPhoto = obj[@"cover_photo_url"];
            
            [bookList addObject:book];
        }];
        
        if ([self.delegate respondsToSelector:@selector(didFetchBookList:)]) {
            [self.delegate didFetchBookList:bookList];
        }
        else
        {
            NSLog(@"did fetch book list.");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if ([self.delegate respondsToSelector:@selector(failToFetchBookListWithError:)]) {
            [self.delegate failToFetchBookListWithError:error];
        }
        else {
            NSLog(@"fail to fetch book list.");
        }
    }];
}

-(void)fetchBookListWithBookID:(NSInteger)bookID
{

}
@end
