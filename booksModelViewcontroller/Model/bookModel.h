//
//  bookModel.h
//  booksModelViewcontroller
//
//  Created by hcc on 2014/2/26.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BookModelDelegate <NSObject>

@optional
// 抓取成功
-(void) didFetchBookList:(NSMutableArray* ) bookList;
// 抓取失敗
-(void) failToFetchBookListWithError:(NSError* ) error;
@end

@interface bookModel : NSObject

@property (nonatomic) NSInteger bookID;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* author;
@property (nonatomic) CGFloat price;
@property (nonatomic, strong) NSString* content;
@property (nonatomic) NSString* coverPhoto;
@property (nonatomic,weak) id<BookModelDelegate> delegate;  // _tableview.delegate = self   非常重要！

-(void) fetchBookList;
-(void) fetchBookListWithBookID:(NSInteger) bookID;

@end
