//
//  ViewController.m
//  booksModelViewcontroller
//
//  Created by hcc on 2014/2/26.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "MainViewController.h"
#import "bookModel.h"

@interface MainViewController ()<BookModelDelegate, UITableViewDataSource, UITableViewDelegate>
{
    bookModel* _bookModel;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray* bookList;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self loadBookList];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
}

-(void) loadBookList
{
    _bookModel = [bookModel new];
    _bookModel.delegate = self;
    [_bookModel fetchBookList];
}

-(void)setBookList:(NSMutableArray *)bookList
{
    _bookList = bookList;
    [self.tableView reloadData];
}
#pragma mark - bookList Delegate
-(void) didFetchBookList:(NSMutableArray* ) bookList
{
    self.bookList = bookList;
}

#pragma mark - datasource delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_bookList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellID = @"bookListCellID";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    }
    bookModel* book = (bookModel *)self.bookList[indexPath.row];
    cell.textLabel.text = book.title;
    cell.detailTextLabel.text = book.author;
    
    return cell;
}

@end
