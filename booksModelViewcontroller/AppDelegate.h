//
//  AppDelegate.h
//  booksModelViewcontroller
//
//  Created by hcc on 2014/2/26.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
